package com.banisaleh.davidhidayat.moviecatalogue3.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.banisaleh.davidhidayat.moviecatalogue3.Adaptor.MovieAdaptor;
import com.banisaleh.davidhidayat.moviecatalogue3.Adaptor.TvShowAdaptor;
import com.banisaleh.davidhidayat.moviecatalogue3.DetailMovieActivity;
import com.banisaleh.davidhidayat.moviecatalogue3.DetailTvShowActivity;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.Genre;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.Movie;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.TvShow;
import com.banisaleh.davidhidayat.moviecatalogue3.R;
import com.banisaleh.davidhidayat.moviecatalogue3.ViewModel.GenreViewModel;
import com.banisaleh.davidhidayat.moviecatalogue3.ViewModel.MovieViewModel;
import com.banisaleh.davidhidayat.moviecatalogue3.ViewModel.TvShowViewModel;

import java.util.ArrayList;

public class TvShowFragment extends Fragment {
    private Context context;
    private RecyclerView rv_tv_show;

    private TvShowAdaptor adapter;
    private ProgressBar progressBar;

    private TvShowViewModel viewModel;
    private GenreViewModel genreViewModel;

    ArrayList<Genre> genres;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        genreViewModel = ViewModelProviders.of(getActivity()).get(GenreViewModel.class);
        genreViewModel.getItem().observe(getActivity(),getGenres);
        genreViewModel.setItem();
        viewModel = ViewModelProviders.of(getActivity()).get(TvShowViewModel.class);
        viewModel.getItem().observe(this,getMovies);
        viewModel.setItem();
        showLoading(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tv_show, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.progressBar);
        context = view.getContext();
        rv_tv_show = view.findViewById(R.id.rv_tv_show);

        adapter = new TvShowAdaptor(getContext());
        adapter.notifyDataSetChanged();
        rv_tv_show.setLayoutManager(new LinearLayoutManager(context));
        rv_tv_show.setAdapter(adapter);
        adapter.setOnItemClickCallback(new TvShowAdaptor.OnItemClickCallback() {
            @Override
            public void onItemClicked(TvShow data) {
                showSelectedMovie(data);
            }
        });


    }
    private void showSelectedMovie(TvShow data){
        Intent detailTvShow= new Intent(getContext(), DetailTvShowActivity.class);
        detailTvShow.putExtra(DetailMovieActivity.EXTRA_DATA, data);
        startActivity(detailTvShow);
    }
    private Observer<ArrayList<TvShow>> getMovies = new Observer<ArrayList<TvShow>>() {
        @Override
        public void onChanged(ArrayList<TvShow> items) {
            if (items != null) {
                adapter.setData(items,genres);
                showLoading(false);
            }
        }
    };
    private Observer<ArrayList<Genre>> getGenres = new Observer<ArrayList<Genre>>() {
        @Override
        public void onChanged(ArrayList<Genre> items) {
            if (items != null) {
                genres =items;
            }
        }
    };
    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }
}