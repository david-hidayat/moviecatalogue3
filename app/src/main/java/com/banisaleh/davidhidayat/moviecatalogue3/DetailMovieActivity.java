package com.banisaleh.davidhidayat.moviecatalogue3;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.banisaleh.davidhidayat.moviecatalogue3.Model.Genre;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.Movie;
import com.banisaleh.davidhidayat.moviecatalogue3.db.MovieHelper;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class DetailMovieActivity extends AppCompatActivity {
    public static final String EXTRA_DATA = "extra_data";
    private TextView txtTittle,txtReleaseDate,txtOverview;
    private ImageView imgPoster;
    private Context context;

    private Movie movie;
    private int position;

    private MovieHelper movieHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);

        context =  this;

        txtTittle   = findViewById(R.id.tv_tittle);
        txtReleaseDate    = findViewById(R.id.tv_release_date);
        txtOverview = findViewById(R.id.tv_overview);
        imgPoster   = findViewById(R.id.img_poster);
        movieHelper = MovieHelper.getInstance(getApplicationContext());
        movieHelper.open();

        movie = getIntent().getParcelableExtra(EXTRA_DATA);
        long result = movieHelper.insertNote(movie);
        Log.e("result DBNYA", String.valueOf(result));
        movieHelper.close();
        txtTittle.setText(movie.getTitle());
        txtReleaseDate.setText(movie.getRelease_date());
        txtOverview.setText(movie.getOverview());

        Glide.with(context)
                .load("https://image.tmdb.org/t/p/w500"+movie.getPoster_path())
                .apply(new RequestOptions().override(500, 500))
                .into(imgPoster);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_change_settings){
            Intent mIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(mIntent);
        }
        return super.onOptionsItemSelected(item);
    }
}
