package com.banisaleh.davidhidayat.moviecatalogue3.Model;

import java.util.ArrayList;

public class GenreList
{    ArrayList<Genre> genres;

    public ArrayList<Genre> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<Genre> genres) {
        this.genres = genres;
    }
}
