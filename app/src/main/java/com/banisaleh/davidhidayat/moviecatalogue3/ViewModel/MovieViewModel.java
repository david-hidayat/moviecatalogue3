package com.banisaleh.davidhidayat.moviecatalogue3.ViewModel;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.banisaleh.davidhidayat.moviecatalogue3.API.ApiRequest;
import com.banisaleh.davidhidayat.moviecatalogue3.API.RetroServer;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.Movie;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.MovieList;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieViewModel extends AndroidViewModel {
    private ApiRequest api;
    private MutableLiveData<ArrayList<Movie>> listItem = new MutableLiveData<>();

    public MovieViewModel(@NonNull Application application) {
        super(application);
    }

    public void setItem() {
        api = RetroServer.getClient().create(ApiRequest.class);
        final ArrayList<Movie> listItems = new ArrayList<>();
        Call<MovieList> GetData = api.getMoviePopular();
        GetData.enqueue(new Callback<MovieList>() {
            @Override
            public void onResponse(Call<MovieList> call, Response<MovieList> response) {
                List<Movie> movieList= response.body().getResults();
                for (int i = 0; i < movieList.size(); i++) {
                    listItems.add(movieList.get(i));
                }
                listItem.postValue(listItems);
            }
            @Override
            public void onFailure(Call<MovieList> call, Throwable t) {
                Toast.makeText(getApplication(), "Koneksi Bermasalah", Toast.LENGTH_LONG).show();
            }
        });
    }
    public MutableLiveData<ArrayList<Movie>> getItem() {
        return listItem;
    }
}
