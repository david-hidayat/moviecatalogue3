package com.banisaleh.davidhidayat.moviecatalogue3.ViewModel;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.banisaleh.davidhidayat.moviecatalogue3.API.ApiRequest;
import com.banisaleh.davidhidayat.moviecatalogue3.API.RetroServer;
import com.banisaleh.davidhidayat.moviecatalogue3.Adaptor.TvShowAdaptor;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.Movie;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.MovieList;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.TvShow;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.TvShowList;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TvShowViewModel extends AndroidViewModel {
    private ApiRequest api;
    private MutableLiveData<ArrayList<TvShow>> listItem = new MutableLiveData<>();

    public TvShowViewModel(@NonNull Application application) {
        super(application);
    }

    public void setItem() {
        api = RetroServer.getClient().create(ApiRequest.class);
        final ArrayList<TvShow> listItems = new ArrayList<>();
        Call<TvShowList> GetData = api.getTvShowPopular();
        GetData.enqueue(new Callback<TvShowList>() {
            @Override
            public void onResponse(Call<TvShowList> call, Response<TvShowList> response) {
                List<TvShow> tvShowList= response.body().getResults();
                for (int i = 0; i < tvShowList.size(); i++) {
                    listItems.add(tvShowList.get(i));
                }
                listItem.postValue(listItems);
            }
            @Override
            public void onFailure(Call<TvShowList> call, Throwable t) {
                Toast.makeText(getApplication(), "Koneksi Bermasalah", Toast.LENGTH_LONG).show();
            }
        });
    }
    public LiveData<ArrayList<TvShow>> getItem() {
        return listItem;
    }
}
