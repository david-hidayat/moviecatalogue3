package com.banisaleh.davidhidayat.moviecatalogue3.db;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.banisaleh.davidhidayat.moviecatalogue3.Model.Movie;

import java.util.ArrayList;
import java.util.List;

//import static com.banisaleh.davidhidayat.moviecatalogue3.db.DatabaseContract.MovieColumns._ID;
import static android.provider.MediaStore.Audio.Playlists.Members._ID;
import static com.banisaleh.davidhidayat.moviecatalogue3.db.DatabaseContract.MovieColumns.ADULT;
import static com.banisaleh.davidhidayat.moviecatalogue3.db.DatabaseContract.MovieColumns.BACKDROP_PATH;
import static com.banisaleh.davidhidayat.moviecatalogue3.db.DatabaseContract.MovieColumns.GENRE_IDS;
//import static com.banisaleh.davidhidayat.moviecatalogue3.db.DatabaseContract.MovieColumns.ID;
import static com.banisaleh.davidhidayat.moviecatalogue3.db.DatabaseContract.MovieColumns.ID;
import static com.banisaleh.davidhidayat.moviecatalogue3.db.DatabaseContract.MovieColumns.ORIGINAL_LANGUAGE;
import static com.banisaleh.davidhidayat.moviecatalogue3.db.DatabaseContract.MovieColumns.ORIGINAL_TITLE;
import static com.banisaleh.davidhidayat.moviecatalogue3.db.DatabaseContract.MovieColumns.OVERVIEW;
import static com.banisaleh.davidhidayat.moviecatalogue3.db.DatabaseContract.MovieColumns.POPULARITY;
import static com.banisaleh.davidhidayat.moviecatalogue3.db.DatabaseContract.MovieColumns.POSTER_PATH;
import static com.banisaleh.davidhidayat.moviecatalogue3.db.DatabaseContract.MovieColumns.RELEASE_DATE;
import static com.banisaleh.davidhidayat.moviecatalogue3.db.DatabaseContract.MovieColumns.TITLE;
import static com.banisaleh.davidhidayat.moviecatalogue3.db.DatabaseContract.MovieColumns.VIDEO;
import static com.banisaleh.davidhidayat.moviecatalogue3.db.DatabaseContract.MovieColumns.VOTE_AVERAGE;
import static com.banisaleh.davidhidayat.moviecatalogue3.db.DatabaseContract.MovieColumns.VOTE_COUNT;
import static com.banisaleh.davidhidayat.moviecatalogue3.db.DatabaseContract.TABLE_MOVIE;

public class MovieHelper {
    private static final String DATABASE_TABLE = TABLE_MOVIE;
    private static DatabaseHelper dataBaseHelper;
    private static MovieHelper INSTANCE;
    private static SQLiteDatabase database;
    private MovieHelper(Context context) {
        dataBaseHelper = new DatabaseHelper(context);
    }
    public static MovieHelper getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (SQLiteOpenHelper.class) {
                if (INSTANCE == null) {
                    INSTANCE = new MovieHelper(context);
                }
            }
        }
        return INSTANCE;
    }
    public void open() throws SQLException {
        database = dataBaseHelper.getWritableDatabase();
    }
    public void close() {
        dataBaseHelper.close();
        if (database.isOpen())
            database.close();
    }
    public ArrayList<Movie> getAllMovies() {
        ArrayList<Movie> arrayList = new ArrayList<>();
        Cursor cursor = database.query(DATABASE_TABLE, null,
                null,
                null,
                null,
                null,
                _ID + " ASC",
                null);
        cursor.moveToFirst();
        Movie movie;
        if (cursor.getCount() > 0) {
            do {
                movie = new Movie();
                movie.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                movie.setPopularity(Float.parseFloat(cursor.getString(cursor.getColumnIndexOrThrow(POPULARITY))));
                movie.setVote_count(Integer.parseInt(cursor.getString(cursor.getColumnIndexOrThrow(String.valueOf(VOTE_COUNT)))));
                movie.setVideo(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndexOrThrow(VIDEO))));
                movie.setPoster_path(cursor.getString(cursor.getColumnIndexOrThrow(POSTER_PATH)));
                movie.setAdult(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndexOrThrow(ADULT))));
                movie.setBackdrop_path(cursor.getString(cursor.getColumnIndexOrThrow(BACKDROP_PATH)));
                movie.setOriginal_language(cursor.getString(cursor.getColumnIndexOrThrow(ORIGINAL_LANGUAGE)));
                movie.setOriginal_title(cursor.getString(cursor.getColumnIndexOrThrow(ORIGINAL_TITLE)));
//                movie.setGenre_ids(cursor.getString(cursor.getColumnIndexOrThrow(GENRE_IDS)));
                movie.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(TITLE)));
                movie.setVote_average(Float.parseFloat(cursor.getString(cursor.getColumnIndexOrThrow(VOTE_AVERAGE))));
                movie.setOverview(cursor.getString(cursor.getColumnIndexOrThrow(OVERVIEW)));
                movie.setRelease_date(cursor.getString(cursor.getColumnIndexOrThrow(RELEASE_DATE)));
                arrayList.add(movie);
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }
    public long insertNote(Movie movie) {
        ContentValues args = new ContentValues();


        args.put(POPULARITY, movie.getPopularity());
        args.put(VOTE_COUNT, movie.getVote_count());
        args.put(VIDEO, movie.isVideo());
        args.put(POSTER_PATH, movie.getPoster_path());
        args.put(ID, movie.getId());
        args.put(ADULT, movie.isAdult());
        args.put(BACKDROP_PATH, movie.getBackdrop_path());
        args.put(ORIGINAL_LANGUAGE, movie.getOriginal_language());
        args.put(ORIGINAL_TITLE, movie.getOriginal_title());
        args.put(GENRE_IDS, getGenres(movie.getGenre_ids()));
        args.put(TITLE, movie.getTitle());
        args.put(VOTE_AVERAGE, movie.getVote_average());
        args.put(OVERVIEW, movie.getOverview());
        args.put(RELEASE_DATE, movie.getRelease_date());
        Log.e("DBNYA",DATABASE_TABLE);
        Log.e("DBNYA",movie.getPoster_path());
        return database.insert(DATABASE_TABLE, null, args);
    }
    public int updateNote(Movie movie) {
        ContentValues args = new ContentValues();
        args.put(POPULARITY, movie.getPopularity());
        args.put(VOTE_COUNT, movie.getVote_count());
        args.put(VIDEO, movie.isVideo());
        args.put(POSTER_PATH, movie.getPoster_path());
//        args.put(ID, movie.getId());
//        args.put(_ID, movie.getId());
        args.put(ADULT, movie.isAdult());
        args.put(BACKDROP_PATH, movie.getBackdrop_path());
        args.put(ORIGINAL_LANGUAGE, movie.getOriginal_language());
        args.put(ORIGINAL_TITLE, movie.getOriginal_title());
        args.put(GENRE_IDS, movie.getId());
        args.put(TITLE, movie.getTitle());
        args.put(VOTE_AVERAGE, movie.getVote_average());
        args.put(OVERVIEW, movie.getOverview());
        args.put(RELEASE_DATE, movie.getRelease_date());
        return database.update(DATABASE_TABLE, args, _ID + "= '" + movie.getId() + "'", null);
    }
    public int deleteNote(int id) {
        return database.delete(TABLE_MOVIE, _ID + " = '" + id + "'", null);
    }

    private int getGenres(List<Integer> genreIds) {
        return genreIds.size();
//        List<Integer> movieGenres = new ArrayList<>();
//        for (Integer genreId : genreIds) {
//                    movieGenres.add(genreId);
//                    break;
//        }
//        return TextUtils.join(", ", movieGenres);
    }

}
