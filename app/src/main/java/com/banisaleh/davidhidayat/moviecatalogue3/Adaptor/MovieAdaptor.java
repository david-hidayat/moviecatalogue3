package com.banisaleh.davidhidayat.moviecatalogue3.Adaptor;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.banisaleh.davidhidayat.moviecatalogue3.Model.Genre;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.Movie;
import com.banisaleh.davidhidayat.moviecatalogue3.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

public class MovieAdaptor extends RecyclerView.Adapter<MovieAdaptor.HolderData> {
    private ArrayList<Genre> allGenres;
    private List<Movie> MovieList ;
    private LayoutInflater mInflater;
    private Context mContext;
    private String overview;

    private OnItemClickCallback onItemClickCallback;


    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }
    public MovieAdaptor(Context context)
    {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.MovieList = new ArrayList<>();
    }
    public void setData(List<Movie> movieList, ArrayList<Genre> genres)
    {
        this.MovieList=movieList ;
        this.allGenres=genres;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public MovieAdaptor.HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_movie,parent,false);
        HolderData holderData = new HolderData(view);
        return holderData;
    }

    @Override
    public void onBindViewHolder(@NonNull final MovieAdaptor.HolderData holder, int position) {
//        if (position%2==0){
//            holder.container.setCardBackgroundColor(Color.rgb(196,196,196));
//        }else{
//            holder.container.setCardBackgroundColor(Color.rgb(143,143,143));
//        }
        final Movie movie = MovieList.get(position);
        float rate = 10 * movie.getVote_average();
        overview = movie.getOverview();
        if (movie.getOverview().length()>110){
            overview = movie.getOverview().substring(0,110)+".....";
        }
        if (rate <= 50){
            holder.pb_rate.setProgressDrawable(mContext.getDrawable(R.drawable.circle_yellow));
        }else{
            holder.pb_rate.setProgressDrawable(mContext.getDrawable(R.drawable.circle_green));
        }
        holder.pb_rate.setProgress((int) rate);
        holder.tv_rate.setText(String.valueOf((int)rate)+"%");
        Glide.with(mContext)
                .load("https://image.tmdb.org/t/p/w500"+movie.getPoster_path())
                .apply(new RequestOptions().override(250, 250))
                .into(holder.img_poster);
        holder.tv_title.setText(movie.getTitle());
//        holder.tv_genre.setText(getGenres(movie.getGenre_ids()));
        holder.tv_popilarity.setText(String.valueOf(movie.getPopularity()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onItemClicked(MovieList.get(holder.getAdapterPosition()));
            }
        });
    }
    public interface OnItemClickCallback {
        void onItemClicked(Movie data);
    }
    @Override
    public int getItemCount() {
        return (MovieList == null) ? 0 : MovieList.size();
    }
    private String getGenres(List<Integer> genreIds) {
        List<String> movieGenres = new ArrayList<>();
        for (Integer genreId : genreIds) {
            for (Genre genre : allGenres) {
                if (genre.getId() == genreId) {
                    movieGenres.add(genre.getName());
                    break;
                }
            }
        }
        return TextUtils.join(", ", movieGenres);
    }
    public class HolderData extends RecyclerView.ViewHolder {
        TextView tv_title,tv_overview,tv_rate,tv_popilarity,tv_genre;
        ImageView img_poster;
        ProgressBar pb_rate;
        CardView container;
        public HolderData(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container_item);
            img_poster = itemView.findViewById(R.id.img_poster);
            tv_title = itemView.findViewById(R.id.title);
            tv_overview = itemView.findViewById(R.id.overview);
            tv_rate = itemView.findViewById(R.id.vote_average);
            tv_genre = itemView.findViewById(R.id.genre);
            tv_popilarity = itemView.findViewById(R.id.popularity);
            pb_rate = itemView.findViewById(R.id.vote_average_bar);
        }
    }

}
