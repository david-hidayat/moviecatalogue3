package com.banisaleh.davidhidayat.moviecatalogue3;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.banisaleh.davidhidayat.moviecatalogue3.Fragment.MovieFavFragment;
import com.banisaleh.davidhidayat.moviecatalogue3.Fragment.MovieFragment;
import com.banisaleh.davidhidayat.moviecatalogue3.Fragment.TvShowFavFragment;
import com.banisaleh.davidhidayat.moviecatalogue3.Fragment.TvShowFragment;
import com.facebook.stetho.DumperPluginsProvider;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.dumpapp.DumperPlugin;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    String sTopNav="movie",sBottomNav="popular";
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        Stetho.initializeWithDefaults(this);
        BottomNavigationView topNav= (BottomNavigationView) findViewById(R.id.top_navigation);
        BottomNavigationView bottomNav  = (BottomNavigationView) findViewById(R.id.navigation);
        topNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_movie:
                        sTopNav = "movie";
                        loadFrame();
                        return true;
                    case R.id.navigation_tv_show:
                        sTopNav = "tv_show";
                        loadFrame();
                        return true;
                }
                return false;
            }
        });
        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_populer:
                        sBottomNav = "popular";
                        loadFrame();
                        return true;
                    case R.id.navigation_favorit:
                        sBottomNav = "favorit";
                        loadFrame();
                        return true;
                }
                return false;
            }
        });

        Fragment fragment;

        fragment = new MovieFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName())
                .commit();



    }
    private void loadFrame(){
        Fragment fragment;
        if(sBottomNav == "popular"){
            if (sTopNav=="movie"){
                fragment = new MovieFragment();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName())
                        .commit();
            }else {
                fragment = new TvShowFragment();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName())
                        .commit();
            }
        }else{
            if (sTopNav=="movie"){
                fragment = new MovieFavFragment();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName())
                        .commit();

            }else {
                fragment = new TvShowFavFragment();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName())
                        .commit();

            }


        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_change_settings){
            Intent mIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(mIntent);
        }
        return super.onOptionsItemSelected(item);
    }
}
