package com.banisaleh.davidhidayat.moviecatalogue3.Fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.banisaleh.davidhidayat.moviecatalogue3.API.ApiRequest;
import com.banisaleh.davidhidayat.moviecatalogue3.API.RetroServer;
import com.banisaleh.davidhidayat.moviecatalogue3.Adaptor.MovieAdaptor;
import com.banisaleh.davidhidayat.moviecatalogue3.DetailMovieActivity;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.Genre;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.Movie;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.MovieList;
import com.banisaleh.davidhidayat.moviecatalogue3.R;
import com.banisaleh.davidhidayat.moviecatalogue3.ViewModel.GenreViewModel;
import com.banisaleh.davidhidayat.moviecatalogue3.ViewModel.MovieViewModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieFragment extends Fragment {
    private Context context;
    private RecyclerView rv_movie;

    private MovieAdaptor adapter;
    private ProgressBar progressBar;

    private MovieViewModel movieViewModel;
    private GenreViewModel genreViewModel;

    ArrayList<Genre> genres;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        genreViewModel = ViewModelProviders.of(getActivity()).get(GenreViewModel.class);
        genreViewModel.getItem().observe(getActivity(),getGenres);
        genreViewModel.setItem();

        movieViewModel = ViewModelProviders.of(getActivity()).get(MovieViewModel.class);
        movieViewModel.getItem().observe(this,getMovies);
        movieViewModel.setItem();
        showLoading(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_movie, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.progressBar);
        context = view.getContext();
        rv_movie = view.findViewById(R.id.rv_movie);

        adapter = new MovieAdaptor(getContext());
        adapter.notifyDataSetChanged();
        rv_movie.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_movie.setAdapter(adapter);
        adapter.setOnItemClickCallback(new MovieAdaptor.OnItemClickCallback() {
            @Override
            public void onItemClicked(Movie data) {
                showSelectedMovie(data);
            }
        });


    }
    private void showSelectedMovie(Movie data){
        Intent detailMovie = new Intent(getContext(), DetailMovieActivity.class);
        detailMovie.putExtra(DetailMovieActivity.EXTRA_DATA, data);
        startActivity(detailMovie);

    }
    private Observer<ArrayList<Movie>> getMovies = new Observer<ArrayList<Movie>>() {
        @Override
        public void onChanged(ArrayList<Movie> items) {
            if (items != null) {
                adapter.setData(items,genres);
                showLoading(false);
            }
        }
    };
    private Observer<ArrayList<Genre>> getGenres = new Observer<ArrayList<Genre>>() {
        @Override
        public void onChanged(ArrayList<Genre> items) {
            if (items != null) {
                genres =items;
            }
        }
    };
    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }
}
