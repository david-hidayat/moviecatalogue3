package com.banisaleh.davidhidayat.moviecatalogue3.db;

import android.provider.BaseColumns;

public class DatabaseContract {
    static String TABLE_MOVIE = "movie";
    static final class MovieColumns implements BaseColumns {
//        static String _ID                = "_idna";


        static String POPULARITY        = "popularity";
        static String VOTE_COUNT        = "vote_count";
        static String VIDEO             = "video";
        static String POSTER_PATH       = "poster_path";
        static String ID                = "id";
        static String ADULT             = "adult";
        static String BACKDROP_PATH     = "backdrop_path";
        static String ORIGINAL_LANGUAGE = "original_language";
        static String ORIGINAL_TITLE    = "original_title";
        static String GENRE_IDS         = "genre_ids";
        static String TITLE             = "title";
        static String VOTE_AVERAGE      = "vote_average";
        static String OVERVIEW          = "overview";
        static String RELEASE_DATE      = "release_date";
    }
    static String TABLE_TV_SHOW = "tv_show";
    static final class Tv_Show_Columns implements BaseColumns {
        static String ORIGINAL_NAME     = "original_name";
        static String GENRE_IDS         = "genre_ids";
        static String NAME              = "name";
        static String POPULARITY        = "popularity";
        static String ORIGIN_COUNTRY    = "origin_country";
        static String VOTE_COUNT        = "vote_count";
        static String FIRST_AIR_DATE    = "first_air_date";
        static String BACKDROP_PATH     = "backdrop_path";
        static String ORIGINAL_LANGUAGE = "original_language";
        static String VOTE_AVERAGE      = "vote_average";
        static String OVERVIEW          = "overview";
        static String POSTER_PATH       = "poster_path";
    }
}
