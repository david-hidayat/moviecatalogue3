package com.banisaleh.davidhidayat.moviecatalogue3.API;


import com.banisaleh.davidhidayat.moviecatalogue3.BuildConfig;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.GenreList;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.MovieList;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.TvShowList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiRequest {
    String API_KEY = BuildConfig.API_KEY;
    @GET("/3/movie/popular?api_key="+API_KEY)
    Call<MovieList> getMoviePopular();
    @GET("/3/genre/movie/list?api_key="+API_KEY)
    Call<GenreList> getMovieGendre();
    @GET("/3/genre/movie/list?api_key="+API_KEY)
    Call<GenreList> getTvShowGendre();
    @GET("/3/tv/popular?api_key="+API_KEY)
    Call<TvShowList> getTvShowPopular();
}
