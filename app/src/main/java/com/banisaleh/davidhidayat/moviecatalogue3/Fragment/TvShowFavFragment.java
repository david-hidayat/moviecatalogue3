package com.banisaleh.davidhidayat.moviecatalogue3.Fragment;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.banisaleh.davidhidayat.moviecatalogue3.R;
import com.banisaleh.davidhidayat.moviecatalogue3.ViewModel.TvShowFavViewModel;

public class TvShowFavFragment extends Fragment {

    private TvShowFavViewModel mViewModel;

    public static TvShowFavFragment newInstance() {
        return new TvShowFavFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tv_show_fav_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(TvShowFavViewModel.class);
        // TODO: Use the ViewModel
    }

}
