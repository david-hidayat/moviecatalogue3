package com.banisaleh.davidhidayat.moviecatalogue3.ViewModel;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.banisaleh.davidhidayat.moviecatalogue3.API.ApiRequest;
import com.banisaleh.davidhidayat.moviecatalogue3.API.RetroServer;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.Genre;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.GenreList;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.Movie;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.MovieList;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GenreViewModel extends AndroidViewModel {
    private ApiRequest api;
    private MutableLiveData<ArrayList<Genre>> listItem = new MutableLiveData<>();

    public GenreViewModel(@NonNull Application application) {
        super(application);
    }

    public void setItem() {
        api = RetroServer.getClient().create(ApiRequest.class);
        final ArrayList<Genre> listItems = new ArrayList<>();
        Call<GenreList> GetData = api.getMovieGendre();
        GetData.enqueue(new Callback<GenreList>() {
            @Override
            public void onResponse(Call<GenreList> call, Response<GenreList> response) {
                List<Genre> movieList= response.body().getGenres();
                for (int i = 0; i < movieList.size(); i++) {
                    listItems.add(movieList.get(i));
                }
                listItem.postValue(listItems);
            }
            @Override
            public void onFailure(Call<GenreList> call, Throwable t) {
                Toast.makeText(getApplication(), "Koneksi Bermasalah", Toast.LENGTH_LONG).show();
            }
        });
    }
    public LiveData<ArrayList<Genre>> getItem() {
        return listItem;
    }
}
