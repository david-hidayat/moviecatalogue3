package com.banisaleh.davidhidayat.moviecatalogue3.Adaptor;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.banisaleh.davidhidayat.moviecatalogue3.Model.Genre;
import com.banisaleh.davidhidayat.moviecatalogue3.Model.TvShow;
import com.banisaleh.davidhidayat.moviecatalogue3.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

public class TvShowAdaptor extends RecyclerView.Adapter<TvShowAdaptor.HolderData> {
    private List<TvShow> showList;
    private ArrayList<Genre> allGenres;
    private LayoutInflater mInflater;
    private Context mContext;
    private OnItemClickCallback onItemClickCallback;
    public TvShowAdaptor(Context context)
    {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.showList = new ArrayList<>();
    }
    public void setData(List<TvShow> showList, ArrayList<Genre> genres)
    {
        this.showList=showList ;
        this.allGenres=genres;
        notifyDataSetChanged();
    }
    public void setOnItemClickCallback( OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    @NonNull
    @Override
    public TvShowAdaptor.HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_tv_show,parent,false);
        HolderData holderData = new HolderData(view);
        return holderData;
    }
    @Override
    public void onBindViewHolder(@NonNull final TvShowAdaptor.HolderData holder, int position) {
        final TvShow tvShow = showList.get(position);

        float rate = 10 * tvShow.getVote_average();

        if (rate <= 50){
            holder.pb_rate.setProgressDrawable(mContext.getDrawable(R.drawable.circle_yellow));
        }else{
            holder.pb_rate.setProgressDrawable(mContext.getDrawable(R.drawable.circle_green));
        }
        holder.pb_rate.setProgress((int) rate);
        holder.tv_rate.setText(String.valueOf((int)rate)+"%");
        Glide.with(mContext)
                .load("https://image.tmdb.org/t/p/w500"+tvShow.getPoster_path())
                .apply(new RequestOptions().override(250, 250))
                .into(holder.img_poster);
        holder.tv_title.setText(tvShow.getName());
        holder.tv_genre.setText(getGenres(tvShow.getGenre_ids()));
        holder.tv_popilarity.setText(String.valueOf(tvShow.getPopularity()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickCallback.onItemClicked(showList.get(holder.getAdapterPosition()));
            }
        });
    }
    public interface OnItemClickCallback {
        void onItemClicked(TvShow data);
    }

    @Override
    public int getItemCount() {
        return (showList == null) ? 0 : showList.size();
    }

    public class HolderData extends RecyclerView.ViewHolder {
        TextView tv_title,tv_overview,tv_rate,tv_popilarity,tv_genre;
        ImageView img_poster;
        ProgressBar pb_rate;
        CardView container;
        public HolderData(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container_item);
            img_poster = itemView.findViewById(R.id.img_poster);
            tv_title = itemView.findViewById(R.id.title);
            tv_overview = itemView.findViewById(R.id.overview);
            tv_rate = itemView.findViewById(R.id.vote_average);
            tv_genre = itemView.findViewById(R.id.genre);
            tv_popilarity = itemView.findViewById(R.id.popularity);
            pb_rate = itemView.findViewById(R.id.vote_average_bar);
        }
    }
    private String getGenres(List<Integer> genreIds) {
        List<String> movieGenres = new ArrayList<>();
        for (Integer genreId : genreIds) {
            for (Genre genre : allGenres) {
                if (genre.getId() == genreId) {
                    movieGenres.add(genre.getName());
                    break;
                }
            }
        }
        return TextUtils.join(", ", movieGenres);
    }

}
