package com.banisaleh.davidhidayat.moviecatalogue3;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.banisaleh.davidhidayat.moviecatalogue3.Model.TvShow;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class DetailTvShowActivity extends AppCompatActivity {
    public static final String EXTRA_DATA = "extra_data";
    private TextView txtTitle,txtFirstAirDate,txtOverview;
    private ImageView imgPoster;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail_tv_show);
        context =  this;

        txtTitle        = findViewById(R.id.tv_title);
        txtFirstAirDate = findViewById(R.id.tv_fisrt_air_date);
        txtOverview     = findViewById(R.id.tv_overview);
        imgPoster       = findViewById(R.id.img_poster);

        TvShow tvShow = getIntent().getParcelableExtra(EXTRA_DATA);

        txtTitle.setText(tvShow.getName());
        txtFirstAirDate.setText(tvShow.getFirst_air_date());
        txtOverview.setText(tvShow.getOverview());

        Glide.with(context)
                .load("https://image.tmdb.org/t/p/w500"+tvShow.getPoster_path())
                .apply(new RequestOptions().override(500, 500))
                .into(imgPoster);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_change_settings){
            Intent mIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(mIntent);
        }
        return super.onOptionsItemSelected(item);
    }
}
